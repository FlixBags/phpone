<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3>Jawaban no. 1</h3>";
        $first_sentence = "Hello PHP!";
        $second_sentence = "I'm ready for the challenges";
		echo "<p>Kalimat pertama: " . $first_sentence ."</p>";
		echo "<p>Jumlah karakter pada kalimat pertama: " .strlen($first_sentence) ."</p>";
		echo "<p>Jumlah kata pada kalimat pertama: " .str_word_count($first_sentence) ."</p>";
		echo "<p>Kalimat kedua: " . $second_sentence ."</p>";
		echo "<p>Jumlah karakter pada kalimat kedua: " .strlen($second_sentence) ."</p>";
		echo "<p>Jumlah kata pada kalimat kedua: " .str_word_count($second_sentence) ."</p>";
        
        echo "<h3>Jawaban no. 2</h3>";
        $string2 = "I love PHP";
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ; 
        echo "Kata ketiga: " . substr($string2, 7, 3) . "<br>" ; 

        echo "<h3>Jawaban no. 3</h3>";
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" "; 
		echo "<p>Kalimat ketiga: " . $string3 ."</p>";
		echo "<p>Ganti kata pada kalimat ketiga: " . str_replace("sexy", "awesome", $string3) ."</p>";
    ?>
</body>
</html>
